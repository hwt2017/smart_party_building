package com.ruoyi.project.system.detail.controller;

import java.util.List;

import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.IUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.detail.domain.UserDetail;
import com.ruoyi.project.system.detail.service.IUserDetailService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 个人履历Controller
 * 
 * @author ruoyi
 * @date 2020-05-14
 */
@Controller
@RequestMapping("/system/detail")
public class UserDetailController extends BaseController
{
    private String prefix = "system/detail";

    @Autowired
    private IUserDetailService userDetailService;
    @Autowired
    private IUserService userService;

    @RequiresPermissions("system:detail:view")
    @GetMapping()
    public String detail(UserDetail detail,ModelMap mmap)
    {
        mmap.put("userId",detail.getUserId());
        return prefix + "/detail";
    }

    /**
     * 查询个人履历列表
     */
    @RequiresPermissions("system:detail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserDetail userDetail)
    {
        startPage();
        List<UserDetail> list = userDetailService.selectUserDetailList(userDetail);
        return getDataTable(list);
    }

    /**
     * 导出个人履历列表
     */
    @RequiresPermissions("system:detail:export")
    @Log(title = "个人履历", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserDetail userDetail)
    {
        List<UserDetail> list = userDetailService.selectUserDetailList(userDetail);
        ExcelUtil<UserDetail> util = new ExcelUtil<UserDetail>(UserDetail.class);
        return util.exportExcel(list, "detail");
    }

    /**
     * 新增个人履历
     */
    @GetMapping("/add")
    public String add(Long userId,ModelMap mmap)
    {
        User user = userService.selectUserById(userId);
        mmap.put("userId",userId);
        mmap.put("userName",user.getUserName());
        return prefix + "/add";
    }

    /**
     * 新增保存个人履历
     */
    @RequiresPermissions("system:detail:add")
    @Log(title = "个人履历", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserDetail userDetail)
    {
        return toAjax(userDetailService.insertUserDetail(userDetail));
    }

    /**
     * 修改个人履历
     */
    @GetMapping("/edit/{detailId}")
    public String edit(@PathVariable("detailId") Long detailId, ModelMap mmap)
    {
        UserDetail userDetail = userDetailService.selectUserDetailById(detailId);
        mmap.put("userDetail", userDetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存个人履历
     */
    @RequiresPermissions("system:detail:edit")
    @Log(title = "个人履历", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserDetail userDetail)
    {
        return toAjax(userDetailService.updateUserDetail(userDetail));
    }

    /**
     * 删除个人履历
     */
    @RequiresPermissions("system:detail:remove")
    @Log(title = "个人履历", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userDetailService.deleteUserDetailByIds(ids));
    }
}
