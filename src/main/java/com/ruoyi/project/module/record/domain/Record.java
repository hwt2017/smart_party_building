package com.ruoyi.project.module.record.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.Date;

/**
 * 会议记录对象 tb_record
 * 
 * @author ruoyi
 * @date 2020-05-09
 */
public class Record extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long recordId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    private String deptName;

    /** 记录人id */
    @Excel(name = "记录人id")
    private Long userId;

    /** 记录人姓名 */
    @Excel(name = "记录人姓名")
    private String userName;

    /** 记录时间 */
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date recordTime;

    /** 会议记录标题 */
    @Excel(name = "会议记录标题")
    private String recordTitle;

    /** 会议记录内容 */
    @Excel(name = "会议记录内容")
    private String recordContent;

    /** 记录图片 */
    @Excel(name = "记录图片")
    private String recordImg;

    /** 记录视频 */
    @Excel(name = "记录视频")
    private String recordVideo;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public void setRecordId(Long recordId)
    {
        this.recordId = recordId;
    }

    public Long getRecordId() 
    {
        return recordId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setRecordTime(Date recordTime) 
    {
        this.recordTime = recordTime;
    }

    public Date getRecordTime() 
    {
        return recordTime;
    }
    public void setRecordTitle(String recordTitle) 
    {
        this.recordTitle = recordTitle;
    }

    public String getRecordTitle() 
    {
        return recordTitle;
    }
    public void setRecordContent(String recordContent) 
    {
        this.recordContent = recordContent;
    }

    public String getRecordContent() 
    {
        return recordContent;
    }
    public void setRecordImg(String recordImg) 
    {
        this.recordImg = recordImg;
    }

    public String getRecordImg() 
    {
        return recordImg;
    }
    public void setRecordVideo(String recordVideo) 
    {
        this.recordVideo = recordVideo;
    }

    public String getRecordVideo() 
    {
        return recordVideo;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("recordId", getRecordId())
            .append("deptId", getDeptId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("recordTime", getRecordTime())
            .append("recordTitle", getRecordTitle())
            .append("recordContent", getRecordContent())
            .append("recordImg", getRecordImg())
            .append("recordVideo", getRecordVideo())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
