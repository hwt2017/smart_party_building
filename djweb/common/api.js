// var domain = 'http://j.jolinmind.cn/dangjian/';
// var domain = 'https://j.jolinmind.cn/party_building_web/';
var domain='https://j.jolinmind.cn/new_building_demo/';

let  address = {
	login: `${domain}api/login`,
	getUserDetail: `${domain}api/getUserDetail`,//成员履历
	getDeptList: `${domain}api/getDeptList`, //组织结构	
	getUserByDeptId: `${domain}api/getUserByDeptId`, //组织成员
	getUserinfo: `${domain}api/getUserInfo`, //组织成员	
	getNewTypeList: `${domain}api/getNewTypeList`, //资讯分类
	getNewsByTypeId: `${domain}api/getNewsByTypeId`, //资讯分类下的资讯新闻
	getNewsInfo: `${domain}api/getNewsInfo`, //资讯详细信息
	getNoticeInfo: `${domain}api/getNoticeInfo`, //资讯公告信息
	getNoticeList: `${domain}api/getNoticeList`, //资讯公告
	getNewsByStatus: `${domain}api/getNewsByStatus`, //资讯审核内容(状态：1 待审核 2 已审核)
	getNoticesByStatus: `${domain}api/getNoticesByStatus`, //公告审核内容 (状态：1 待审核 2 已审核)
	isReviewer: `${domain}api/isReviewer`, //判断是否是审核人
	approvalNews: `${domain}api/approvalNews`, //资讯审核 (传值 newId 和 status=2)
	approvalNotice: `${domain}api/approvalNotice`, //公告审核 (传值noticeId和status=2)
	getInspectList: `${domain}api/getInspectList`, //领导视察
	getInspectInfo: `${domain}api/getInspectInfo`, //领导视察详情
	getFeelList: `${domain}api/getFeelList`, //心得体会列表
	getFeelInfo: `${domain}api/getFeelInfo`, //心得体会详情
	getExchangeList: `${domain}api/getExchangeList`, //单位交流
	getExchangeInfo: `${domain}api/getExchangeInfo`, //单位交流详情
	getDeedList: `${domain}api/getDeedList`, //典型事迹
	getDeedInfo: `${domain}api/getDeedInfo`, //典型事迹详情
	getAwardList: `${domain}api/getAwardList`, //荣誉奖项
	getAwardInfo: `${domain}api/getAwardInfo`, //荣誉奖项详情
	getArticleList: `${domain}api/getArticleList`, //分享美文列表
	getArticleInfo: `${domain}api/getArticleInfo`, //分享美文详情
	getLightInfo: `${domain}api/getLightInfo`, //十大亮点详情
	getLightList: `${domain}api/getLightList`, //十大亮点
	getPlanInfo: `${domain}api/getPlanInfo`, //学习计划详情
	getPlanList: `${domain}api/getPlanList`, //学习计划列表 自己提交的
	getProblemInfo: `${domain}api/getProblemInfo`, //问题墙详情
	getProblemList: `${domain}api/getProblemList`, //问题墙列表
	getRecordInfo: `${domain}api/getRecordInfo`, //会议记录详情
	getRecordList: `${domain}api/getRecordList`, //会议记录列表
	getStoryInfo: `${domain}api/getStoryInfo`, //暖心故事详情
	getStoryList: `${domain}api/getStoryList`, //暖心故事
	getTaleInfo: `${domain}api/getTaleInfo`, //小组故事详情
	getTaleList: `${domain}api/getTaleList`, //小组故事列表
	removePlan: `${domain}api/removePlan`, //学习计划删除
	upload: `${domain}api/upload`, //上传
	addRecord: `${domain}api/addRecord`, //会议记录天添加
	addPlan: `${domain}api/addPlan`, //学习计划添加
	addProblem: `${domain}api/addProblem`, //问题墙添加
	addFeel: `${domain}api/addFeel`, //心得体会增加
	addArticle: `${domain}api/addArticle`, //分享美文添加
	addTale: `${domain}api/addTale`, //小组故事添加
	getNewsList: `${domain}api/getNewsList`, //最新资讯
	getDeptRankList: `${domain}api/getDeptRankList`, //小组学分排行
	getUserRankList: `${domain}api/getUserRankList`, //成员学分排行
	getLearnInfo: `${domain}api/getLearnInfo`, //成员学分记录
	addLearn: `${domain}api/addLearn`, //学分添加
	getPictureList: `${domain}api/getPictureList`, //轮播图片
	resetPassword: `${domain}api/resetPassword`, //修改密码
	editAvatar: `${domain}api/editAvatar`, //修改头像
	getPagerInfo: `${domain}api/getPagerInfo`, //试卷详情
	getPagerList: `${domain}api/getPagerList`, //考试列表 userId 考试对象
	addPagerAnswer: `${domain}api/addPagerAnswer`, //添加考试答案
	getViolateList: `${domain}api/getViolateList`, //违规违纪列表
	getViolateInfo: `${domain}api/getViolateInfo`, //
	selectVoteList: `${domain}vote_api/selectVoteList`, //选举任务列表
	getVoteResult: `${domain}vote_api/getVoteResult`, //选举结果
	getDeptPicture: `${domain}app_api/getDeptPicture`, //结构图
}
export {
	address
}
